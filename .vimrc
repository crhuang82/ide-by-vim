
" ********  前置設定 *******
 let mapleader=";"                               " 設置快捷鍵前缀 <Leader>
 autocmd BufWritePost $MYVIMRC source $MYVIMRC   " 讓配置立即生效


" ***************  vundle 環境設置  ********************************
filetype off   " 必須
set rtp+=~/.vim/bundle/Vundle.vim

" vundle 插件列表開始
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'                   " 讓vundle管理插件版本,必須

" airline
Plugin 'vim-airline/vim-airline'                " https://github.com/vim-airline/vim-airline
Plugin 'vim-airline/vim-airline-themes'         " https://github.com/vim-airline/vim-airline-themes

" Taglist
Plugin 'vim-scripts/taglist.vim'                " https://github.com/vim-scripts/taglist.vim

" Mark--Karkat 反白當前的字
Plugin 'vim-scripts/Mark--Karkat'               " https://github.com/vim-scripts/Mark--Karkat

" C/C++ 語法highlight
Plugin 'octol/vim-cpp-enhanced-highlight'       " https://github.com/octol/vim-cpp-enhanced-highlight

" 起始畫面 Startify
Plugin 'mhinz/vim-startify'                     " https://github.com/mhinz/vim-startify

" 模糊Fuzzy尋找檔案
Plugin 'ctrlpvim/ctrlp.vim'                     " https://github.com/ctrlpvim/ctrlp.vim

" 插件列表結束
call vundle#end()
filetype plugin indent on   " 必須 加載vim自帶和插件相應的語法和文件類型相關腳本
" *************************************************************


" **** airline ****
set laststatus=2                                " 永遠(2)開啟狀態列
let g:airline_powerline_fonts = 1               " 使用power line 字形
set t_Co=256

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

let g:airline_left_sep = '▶'
let g:airline_right_sep = '◀'

" tabline setting
let g:airline#extensions#tabline#enabled = 1        " enable tabline
let g:airline#extensions#tabline#buffer_nr_show = 1 " 顯示編號
let g:airline#extensions#tabline#fnamemod = ':t'    " 只顯示檔名在tabline上

nnoremap <Leader>B :bp<CR>                          " 下一個buffer
nnoremap <Leader>b :bn<CR>                          " 上一個buffer
nnoremap <Leader>q :bd<CR>                          " 關掉目前buffer

nnoremap <Leader>1 :b 1<CR>                         " 跳到第一個buffer
nnoremap <Leader>2 :b 2<CR>                         "       二
nnoremap <Leader>3 :b 3<CR>
nnoremap <Leader>4 :b 4<CR>
nnoremap <Leader>5 :b 5<CR>
nnoremap <Leader>6 :b 6<CR>
nnoremap <Leader>7 :b 7<CR>
nnoremap <Leader>8 :b 8<CR>
nnoremap <Leader>9 :b 9<CR>
nnoremap <Leader>0 :b 10<CR>

" **** airline theme ****
let g:airline_theme='badwolf'                       " 使用badwolf主題

" **** ctag ****
set tags=./tags;                                    " 讓ctags可以一層一層往上找是否有tags file

" **** cscope ****
if has ("cscope")
    let i = 1
    while i < 20
        if filereadable("cscope.out")
            let db = getcwd()."/cscope.out"
	    " echo db
	    let $CSCOPE_DB = db
            cs add $CSCOPE_DB
            let i = 20
        else
            cd ..
        let i += 1
        endif
    endwhile

    set csto=0                                                                  " 先找DB,搜尋不到再找 tag file
    set cst                                                                     " 同時找db and tag file
    nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR> :copen<CR><CR>       " 查詢symbol
    nmap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR> :copen<CR><CR>       " 查詢egrep匹配
    nmap <C-\>c :cs find e <C-R>=expand("<cword>")<CR><CR> :copen<CR><CR>       " 查詢呼叫到此函數的函數
    set cscopequickfix=c-,d-,e-,g-,i-,s-,t-                                     " use quickfix to show the search result
endif


" **** taglist ****
let Tlist_Show_One_File=1                           " 只顯示當下文件的tag
let Tlist_Exit_OnlyWindow=1                         " 文件退出後,也會退出taglist
" let Tlist_Auto_Open=1                             " 啟動vim後,自動開啟taglist
map <silent> <F8> :TlistToggle<cr>                  " 使用F8啟動以及關閉tag list視窗
let Tlist_WinWidth = 80                             " 視窗寬度設定80

" **** Startify ****
nmap <F12> :Startify<CR>

" **** ctrlp ****
set wildignore+=*/tmp/*,*.so,*.swp,*.zip

" 定義不找的附檔名
let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/]\.(git|hg|svn|rvm|repo)$',
    \ 'file': '\v\.(exe|so|dll|zip|tar|tar.gz|pyc)$',
    \ }


"""""""""""""""" VIM 內建 pluging 設定 """""""""""""""""""""""""""""""
" ++++ netrw ++++
nmap <silent> <leader>fe :Explore<cr>               " 瀏覽當下檔案的資料夾

" ++++ quickfix ++++
nmap <F9> :cnext<CR>                                " 跳到下一個結果
nmap <F10> :cprev<CR>                               " 跳到上一個結果
nmap <silent><leader>co :copen<CR>                  " 開啟quickfix
nmap <silent><leader>ccl :cclose<CR>                " 關閉quickfix
" col              " 前一個qf
" cnew             " 後一個gf
" 當離開檔案後.自動端關閉quickfix window
aug QFClose
  au!
  au WinEnter * if winnr('$') == 1 && getbufvar(winbufnr(winnr()), "&buftype") == "quickfix"|q|endif
aug END

" ========= UI ==========
set number                                          " 顯示行號
set cursorline                                      " 高亮顯示當前列
"set cursorcolumn                                   " 高亮顯示當前行
set scrolloff=7                                     " 上下各留7行
colorscheme desert
syntax on

" ======== 編輯 ==============
nnoremap <F3> :set list! list?<CR>                  " 顯示換行符號
nmap <silent> <F2> :set invpaste<CR>:set paste?<CR> " 避免貼上時出現亂排
imap <silent> <F2> <ESC>:set invpaste<CR>:set paste?<CR>

" ======= 搜尋 ===========
set incsearch                                       " 及時跳到搜尋的字
set ignorecase                                      " 不分大小寫


" ======== Setting ======
" 1.  回到檔案最後開啟的位置
 if has("autocmd")
    autocmd BufRead *.txt set tw=78
    autocmd BufReadPost *
    \ if line("'\"") > 0 && line ("'\"") <= line("$") |
    \   exe "normal g'\"" |
    \ endif
endif

" 2. 列出目前的buffer
nnoremap gb :ls<CR>:b<Space>


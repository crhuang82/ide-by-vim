#!/bin/bash

# show souce code path color
COLOR_REST='\e[0m'
COLOR_GREEN='\e[0;32m';                                                                                                                                                          
COLOR_RED='\e[1;31m';

if [ "${1}" == "abc" ]; then                                                                                                                                                  
  PROJECT=abc
elif [ "${1}" == "efg" ]; then
  PROJECT=qwe/efg
else
  PROJECT=abc
fi

#echo $PROJECT

declare -a folderPath=(
  "/hardware/ril/"
  "/frameworks/opt/telephony/src/"
  "/vendor/xxx/$PROJECT/proprietary/zzzzz"
  "/vendor/yyy/dddd/aaa.cpp"
)

OUT_PATH=${PWD}
SRC_PATH=${PWD}

if [ -d ${OUT_PATH} ]; then
    echo "Use current directory \"${OUT_PATH}}\""
else
    echo "mkdir \"${OUT_PATH}\""
    mkdir ${OUT_PATH}
fi

rm ${OUT_PATH}/names.file
for i in "${folderPath[@]}"
do
  if [ -d "${SRC_PATH}${i}" ]; then
    find -L ${SRC_PATH}${i} -name "*.h" -o -name "*.cpp" -o -name "*.cc" -o -name "*.c" -o -name "*.java" -o -name "*.mk" -o -name "*.min"|grep -vi '\.git' >>${OUT_PATH}/names.file
    echo -e "${COLOR_GREEN}\"$i\" scann finish.${COLOR_REST}"
  else
    if [ -e "${SRC_PATH}${i}" ]; then
      find -L ${SRC_PATH}${i} -name "*.h" -o -name "*.cpp" -o -name "*.cc" -o -name "*.c" -o -name "*.java" -o -name "*.mk" -o -name "*.min"|grep -vi '\.git' >>${OUT_PATH}/names.file
      echo -e "${COLOR_GREEN}\"$i\" scann finish.${COLOR_REST}"
    else
      echo -e "${COLOR_RED}[WARNING] \"$i\" NOTTT exist.${COLOR_REST}"
    fi  
  fi  
done

echo "For create ctags databases.."
ctags -L ${OUT_PATH}/names.file -f ${OUT_PATH}/tags

echo "For create cscope databases.."
cscope -Rbq -i ${OUT_PATH}/names.file

echo "DONE!!"
